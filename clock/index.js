const worker = function() {
    let time = new Date();
    document.getElementById('hour').innerHTML = time.getHours();
    document.getElementById('minute').innerHTML = time.getMinutes();
    document.getElementById('second').innerHTML = time.getSeconds();
    setTimeout(worker, 1000);
}
worker()